package io;
import AucitonService.AuctionDetails;
import java.util.Scanner;

public class AuctionCreator {
    private Scanner sc = new Scanner(System.in);

    public void close(){
        sc.close();

    }
    public AuctionDetails createAuction(){
        System.out.print("Tytuł: ");
        String title = sc.nextLine();
        System.out.print("Opis: ");
        String description = sc.nextLine();
        System.out.print("Cena: ");
        int price = sc.nextInt();

        return new AuctionDetails(title, description,price);
    }
}
