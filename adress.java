package AucitonService;

public class adress {
    private String city;
    private String street;
    private String postalCode;
    private String homeNo;
    private int flatno;

    public String getAdressInfo(){
        return city + street + postalCode + homeNo + flatno;
    }

    public adress(String city, String street, String postalCode, String homeNo, int flatno) {
        this.city = city;
        this.street = street;
        this.postalCode = postalCode;
        this.homeNo = homeNo;
        this.flatno = flatno;
    }

    private String getCity() {
        return city;
    }

    private void setCity(String city) {
        this.city = city;
    }

    private String getStreet() {
        return street;
    }

    private void setStreet(String street) {
        this.street = street;
    }

    private String getPostalCode() {
        return postalCode;
    }

    private void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    private String getHomeNo() {
        return homeNo;
    }

    private void setHomeNo(String homeNo) {
        this.homeNo = homeNo;
    }

    private int getFlatno() {
        return flatno;
    }

    private void setFlatno(int flatno) {
        this.flatno = flatno;
    }
}
