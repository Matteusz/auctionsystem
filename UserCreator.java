package io;

import AucitonService.UserData;
import AucitonService.adress;
import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;


public class UserCreator {

    private Scanner sc = new Scanner(System.in);

    public void close() {
        sc.close();
    }

    public UserData createUser() {
        System.out.print("Imię: ");
        String name = sc.nextLine();
        System.out.print("Nazwisko: ");
        String surname = sc.nextLine();
        System.out.print("Numer telefonu: ");
        int phoneNumber = sc.nextInt();
        System.out.print("Wprowadż wiek: ");
        int age = sc.nextInt();
        if(age < 18) {
            System.out.println("Jesteś za młody by stworzyć konto");
            System.exit(0);
        }
        Random random = new Random();
        int accID = random.nextInt(20000);
        System.out.println("Twoje ID: " + accID);

        return new UserData(name, surname, phoneNumber, age, accID);


    }
}
